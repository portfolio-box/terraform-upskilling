resource "aws_subnet" "private-subnet-in-ap-south-1a" {
  vpc_id            = "${aws_vpc.default.id}"
  cidr_block        = "${var.private_subnet_cidr}"
  availability_zone = "ap-south-1a"
  tags = {
    Name = "TF Private Subnet"
  }
}
