resource "aws_subnet" "public-subnet-in-ap-south-1a" {
  vpc_id            = "${aws_vpc.default.id}"
  cidr_block        = "${var.public_subnet_cidr}"
  availability_zone = "ap-south-1a"
  tags = {
    Name = "TF Public Subnet"
  }
}

