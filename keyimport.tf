resource "aws_key_pair" "TFKey" {
  key_name   = "TFPrimaryKey"
  public_key = "${file("${var.key_path}")}"
}
