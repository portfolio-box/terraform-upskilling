# Terraform-Upskilling

Learning 3 tier and 4 tier Application IAAC (Infrastructure as a Code)

### Services that will be created
#### Network
1) VPC
2) 2 Subnet (Private - DB Instance and Public - Web Server)
3) 2 Route
4) 2 EIP
5) 3 Security group
6) Gateway

#### Instance
7) NAT Instance
8) Web Server Instance
9)  DB Server Instance
10) SSH Key

