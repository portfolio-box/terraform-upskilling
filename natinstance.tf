resource "aws_instance" "nat" {
  ami                         = "ami-00b3aa8a93dd09c13" # this is a special ami preconfigured to do NAT
  availability_zone           = "ap-south-1a"
  instance_type               = "t2.micro"
  key_name                    = "${var.key_name}"
  vpc_security_group_ids      = ["${aws_security_group.nat.id}"]
  subnet_id                   = "${aws_subnet.public-subnet-in-ap-south-1a.id}"
  associate_public_ip_address = true
  source_dest_check           = false

  tags = {
    Name = "NAT instance"
  }
}
