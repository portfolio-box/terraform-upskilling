variable "access_key" {
  description = "AWS Access Key for Terraform"
  default     = ""
}

variable "secret_key" {
  description = "AWS Secret Key for Terraform"
  default     = ""
}

variable "region" {
  description = "AWS Region for hosting my network"
  default     = "ap-south-1"
}

variable "key_path" {
  description = "TF Default Key"
  default     = "TFKey.pub"
}

variable "key_name" {
  description = "Key Name for SSH"
  default     = "TFPrimaryKey"
}

variable "vpc_cidr" {
  description = "CIDR for VPC"
  default     = "10.0.0.0/16"
}

variable "public_subnet_cidr" {
  description = "CIDR for Public Subnet"
  default     = "10.0.1.0/24"
}

variable "private_subnet_cidr" {
  description = "CIDR for Private Subnet"
  default     = "10.0.2.0/24"
}

variable "amis" {
  description = "Base AMI to Launch the Instances"
  default = {
    ap-south-1 = "ami-0d2692b6acea72ee6"
    # ap-south-2 = "ami-0d2692b6acea72ee6"
  }
}
